(req, res) => {
  if (!req.body || !req.body.action || !req.body.action.type) return res.send("Invalid data. Plz use Trello Webhooks.");
  let { action, model } = req.body;
  let data = {};
  let conf = ChannelConfig.FindByBoard(action.data.board.id);
  if (!conf) return res.send(`No channel has events for the Trello board ${action.data.board.name}`);
  let { channelId, disabledEvents } = conf;
  data.channel = bot.channels.get(channelId);
  if (disabledEvents.includes(action.type)) {
    return res.send(`This event is disabled. Get rekt.`);
  }
  return WebhookUser.fetchWebhook(channelId).then(webhook => {
    res.send(`Doing something with the event ${action.type}`);
    Events.use(data.channel, webhook, action, model);
  });
}