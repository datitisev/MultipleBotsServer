(req, res, next) => {
  const event = req.headers['x-github-event'];
  const secret = req.headers['x-hub-signature'];
  const data = req.body;

  if (!event || !data || !data.repository) return res.status(403).send('INVALID DATA. PLZ USE GITHUB WEBHOOKS');

  Log.debug(`Got a \`${event}\` from ${data.repository.full_name}`);

  switch (event) {
    case 'push': {
      GithubEvents.Push(data);
      break;
    }
    case 'release': {
      GithubEvents.Release(data);
      break;
    }
    case 'issues': {
      GithubEvents.Issues(data);
      break;
    }
    case 'issue_comment': {
      GithubEvents.IssueComment(data);
      break;
    }
    case 'pull_request': {
      GithubEvents.PullRequest(data);
      break;
    }
    case 'watch': {
      GithubEvents.Watch(data);
      break;
    }
    case 'fork': {
      GithubEvents.Fork(data);
      break;
    }
    case 'create': {
      GithubEvents.Branch(event, data);
      break;
    }
    case 'delete': {
      GithubEvents.Branch(event, data);
      break;
    }
    case 'ping': {
      GithubEvents.Ping(data);
      break;
    }
    case 'repository': {
      GithubEvents.Repository(data);
      break;
    }
    case 'member': {
      GithubEvents.Member(data);
      break;
    }
    case 'status': {
      GithubEvents.Status(data);
      break;
    }
    case 'gollum': {
      GithubEvents.Gollum(data);
      break;
    }
    default: {
      res.send(`The event ${event} isn't being handled. Sorry!`);
    }
  }

  if (res.headersSent) return false;
  res.send(`Dealing with the webhook's action, ${event}. Sigh...`);
}