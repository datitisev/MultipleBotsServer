#!/bin/bash

echo ===== SETUP =====
echo " => CREATING BOTS FOLDER"
mkdir -pv bots

echo " => REMOVING ROUTES"
rm -rf routes/*


echo ===== DOWNLOADING BOTS =====
echo " => Yappy, the Github Monitor"
if test -d bots/Yappy
then
  ( cd bots/Yappy && git pull -q )
else
  git clone https://github.com/datitisev/DiscordBot-Yappy.git bots/Yappy -q
fi

echo " => Yappy, the Trello Monitor"
if test -d bots/YappyTrello
then
  ( cd bots/YappyTrello && git pull )
else
  git clone https://github.com/datitisev/DiscordBot-YappyTrello.git bots/YappyTrello -q
fi


echo ===== INSTALLING DEPENDENCIES =====
echo " => General Dependencies"
npm install  --no-optional --silent
for dir in `ls bots`
do
  ( echo " => Bot: $dir..." && cd bots/$dir && npm install --no-optional --silent )
done
