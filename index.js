const fs = require('fs');
const path = require('path');
const { spawn, exec } = require('child_process');

const loadFolder = (folder) => {
  return new Promise((resolve, reject) => {
    let folderPath = path.resolve(__dirname, folder);

    fs.readdir(folderPath, (err, files) => {
      if (err) return reject(err);

      let paths = files.map(e => path.resolve(folderPath, e));

      resolve({ files, paths });
    });

  });
}

let build = spawn('./build.sh', {
  cwd: __dirname,
  env: process.env
});
build.stdout.on('data', (data) => {
  data = data.toString();
  if (data.length < 3) return false;
  process.stdout.write(data);
});
build.stderr.on('data', (data) => {
  data = data.toString();
  if (data.length < 3) return false;
  process.stderr.write(data);
});
build.on('close', (code) => {
  if (code !== 0) {
    console.log('\nThe build failed :/');
    process.exit();
  } else {
    const forever = require('forever-monitor');
    const web = require('./web');
    web.setup();
    web.listen();

    loadFolder('bots').then(({ files, paths }) => {
      const processEnv = process.env;
      processEnv['WEB_NO_STANDALONE'] = true;

      files.forEach((file, i) => {
        let child = new (forever.Monitor)(paths[i], {
          max: 500,
          minUptime: 2000,
          spinSleepTime: 1000,
          args: [],
          env: processEnv,
          cwd: `./bots/${file}`
        });

        child.on('start', () => {
          console.log(`===== START LOG =====`);
          console.log(`${files[i]} has started`);
        })

        child.on('exit', () => {
          console.log(`===== EXIT LOG =====`);
          console.log(`${files[i]} has exited`);
        });

        child.on('restart', () => {
          Object.keys(require.cache).forEach(key => delete require.cache[key]);

          console.log(`===== RESTART LOG =====`)
          console.error(`Restarting ${files[i]}...`);
        });

        child.on('watch:restart', (info) => {
          console.log(`===== RESTART LOG =====`)
          console.error(`Restarting ${files[i]} because 1+ files changed.`);
        });

        child.start();
      });
    }).catch(console.error);
  }
});

// process.on('exit', () => {
  // const forever = require('forever-monitor');
// });
