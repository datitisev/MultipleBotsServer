const path = require('path');
const express = require('express');
const exphbs  = require('express-handlebars');
const bodyParser = require('body-parser');
const Storage = require('fs-storage');

const port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080;
const ip = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || '127.0.0.1';

class Web {
  constructor() {
    this.app = express();
    let routesPath = path.resolve(__dirname, './routes')
    this.routes = new Storage(routesPath);
  }

  setup() {
    this.app.use(bodyParser.json({
      limit: '250kb'
    }));

    this.app.use((req, res, next) => {
      if (!["GET", "POST", "ALL"].includes(req.method)) return res.status(401).send(`Invalid Method ${req.method}`);

      let route = this.routes.getItem(`${req.method}${req.path}.js`);
      if (!route) return res.status(404).send(`Not Found`);
      let data = eval(this.routes);
      route(req, res, next);
    });

    this.get('/test', (req, res) => res.json(`GET /test 200`));
    this.post('/test', (req, res) => res.json(`POST /test 200`));
  }

  get(route, func) {
    let data = func.toString();
    this.routes.setItem(`GET${route}.js`, data);
  }

  post(route, func) {
    let data = func.toString();
    this.routes.setItem(`POST${route}.js`, data);
  }

  all(route, func) {
    let data = func.toString();
    this.routes.setItem(`ALL${route}.js`, data);
  }

  listen() {
    this.app.listen(port, ip, (err) => {
      if (err) return false;
      console.log(` => Listening on ${ip || 'localhost'}:${port}`);
    });
  }
}

module.exports = new Web();
